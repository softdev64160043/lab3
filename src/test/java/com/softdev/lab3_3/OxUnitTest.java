/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.softdev.lab3_3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Jib
 */
public class OxUnitTest {

    public OxUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinNoPlayBY_o() {
        String[][] table = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "O";
        assertEquals(false, Ox.checkWin(table,currentPlayer));
        
    }
    @Test
    public void testCheckWinRow2BY_o_output_true() {
        String[][] table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        String currentPlayer = "O";
        assertEquals(true, Ox.checkWin(table,currentPlayer));
        
    }
    @Test
    public void testCheckWinRow1BY_o_output_true() {
        String[][] table = {{"O","O","O"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "O";
        assertEquals(true, Ox.checkWin(table,currentPlayer));
        
    }
    @Test
    public void testCheckWinRow3BY_o_output_true() {
        String[][] table = {{"-","-","-"},{"-","-","-"},{"O","O","O"}};
        String currentPlayer = "O";
        assertEquals(true, Ox.checkWin(table,currentPlayer));
        
    }
}
